# Camera Cloud

Application mobile réalisée avec le framework **NativeScript-Vue** illustrant l'utilisation du composant natif Camera, l'interaction avec le système de fichier local et l'envoi de fichier vers le service de stockage ImgBB (https://fr.imgbb.com/).

Projet librement inspiré de l'exemple fourni par _Raymond Camden_ (https://www.raymondcamden.com/2018/11/15/working-with-the-camera-in-a-nativescript-vue-app).

## Installation des dépendances NPM

Commande à executer dans le terminal à la racine du projet avant la première utilisation.

> npm i

ou

> yarn

### Installation spécifique pour iOS

Le composant NativeScipt nativescript-imagepicker peut dysfonctionner sur iOS. Une version spécifique existe.

> tns plugin add nativescript-noice-image-picker

https://market.nativescript.org/plugins/nativescript-noice-image-picker

Le code lié au composant Picker doit être adapté en conséquence :

var noiceImagePicker = require("nativescript-noice-image-picker");

let args = {
    imageLimit: 3,
    doneButtonTitle: 'Done'
};
// this function returns a promise.
noiceImagePicker.showPicker(args).then(images => {
    images.forEach(img => {
        this.images.push(img);
    })
})

## Service de stockage ImgBB

### Clé d'API

Créer un compte gratuit sur le service de stockage d'images ImgBB et renseigner la clé d'API au sein de la méthode **"upload"** du fichier :

> ./components/Home.vue :

La clé d'API est renseignée ici : https://api.imgbb.com/

_(pour accéder à cette page depuis le tableau de bord de ImgBB, cliquer sur le lien "A propos" se trouvant sur la gauche du menu)._

- Renseigner directement la valeur de la clé d'API dans le code du fichier

> ./components/Home.vue

> const api_key = "";

### Exemple d'upload avec l'API d'ImgBB

#### Exemple de Requête

> curl --location --request POST "https://api.imgbb.com/1/upload?key=YOUR_CLIENT_API_KEY" --form "image=R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"

#### Exemple de Résultat

{
"data": {

    "id": "2ndCYJK",
    "url_viewer": "https://ibb.co/2ndCYJK",
    "url": "https://i.ibb.co/w04Prt6/c1f64245afb2.gif",
    "display_url": "https://i.ibb.co/98W13PY/c1f64245afb2.gif",
    "title": "c1f64245afb2",
    "time": "1552042565",
    "image": {
      "filename": "c1f64245afb2.gif",
      "name": "c1f64245afb2",
      "mime": "image/gif",
      "extension": "gif",
      "url": "https://i.ibb.co/w04Prt6/c1f64245afb2.gif",
      "size": 42
    },
    "thumb": {
      "filename": "c1f64245afb2.gif",
      "name": "c1f64245afb2",
      "mime": "image/gif",
      "extension": "gif",
      "url": "https://i.ibb.co/2ndCYJK/c1f64245afb2.gif",
      "size": "42"
    },
    "medium": {
      "filename": "c1f64245afb2.gif",
      "name": "c1f64245afb2",
      "mime": "image/gif",
      "extension": "gif",
      "url": "https://i.ibb.co/98W13PY/c1f64245afb2.gif",
      "size": "43"
    },
    "delete_url": "https://ibb.co/2ndCYJK/670a7e48ddcb85ac340c717a41047e5c"

},
"success": true,
"status": 200
}

### Images uploadées

Consulter vos images uploadées à l'adresse :

> https://votre-identifiant.imgbb.com/

## Test de l'application

> tns run android

ou

> tns run ios

## Auteur

Alexandre Leroux
(alex@sherpa.one)

Enseignant à l'Université Lorraine :

- IUT Charlemagne (LP Ciasie)
- Institut des Sciences du Digital (Master Sciences Cognitives)

Dépôt Git : https://bitbucket.org/sherpalab/nativescript-vue-camera-cloud/

Mars 2020

Copyright 2020 Alexandre Leroux (alex@sherpa.one)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
